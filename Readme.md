# Accident detection using machine learning

This project seeks to predict places of occurrence of accidents, through the Devimed S.A. concession.

To achieve this, some techniques for anomaly detection will be explored.

To run the notebooks, you must have **python > 3.5.0** installed, and it is recommended to create and activate a virtual environment, after that, follow these steps:

1. Clone this repository  
   `git clone https://gitlab.com/jdiego06/final-project-ml`

2. Navigate to the project folder.  
   `cd final-project-ml`

3. Install requirements.  
   `pip3 install -r requirements.txt `

4. Open Jupyter lab or Jupyter notebook  
   `jupyter-lab .`  
   `jupyter-notebook .`
